package archive

import (
	"fmt"

	"gitlab.com/ilmikko/toolbox/types/archive/compression"
	"gitlab.com/ilmikko/toolbox/types/archive/compressor"
)

type Config struct {
	Default struct {
		Archive     compression.Compression
		Compression compression.Compression
	}
}

type Archive struct {
	cfg         *Config
	compressors map[compression.Compression]compressor.Compressor
}

func (a *Archive) Compress(files ...string) ([]string, error) {
	fs, err := a.CompressWith(a.cfg.Default.Compression, files...)
	if err != nil {
		return fs, err
	}
	return fs, nil
}

func (a *Archive) CompressWith(c compression.Compression, files ...string) ([]string, error) {
	comp, ok := a.compressors[c]
	if !ok {
		return nil, fmt.Errorf("No compressor found for compression %q", c)
	}
	fs, err := comp.CompressAll(files...)
	if err != nil {
		return fs, err
	}
	return fs, nil
}

func New() *Archive {
	a := &Archive{}
	a.cfg = NewConfig()
	a.compressors = compressor.Defaults
	return a
}
