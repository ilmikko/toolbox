package md5

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	MD5RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	md5sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.md5sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, MD5RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.md5sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, MD5RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.md5sum = multicommand.New()
	{
		md5sum := command.New("md5sum", "--binary")
		c.md5sum.Commands = append(c.md5sum.Commands, md5sum)
	}
	return c
}
