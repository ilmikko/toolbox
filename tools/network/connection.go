package network

// Connection will report whether a connection is OK, or, in case of an error,
// what kind of an error it is.
// The connection check consists of a single HTTP request, and will succeed if
// there is any kind of a response (even in case of a 4xx or a 5xx).
func (n *Network) Connection(host string) (bool, error) {
	if err := n.Command.Connected(host); err != nil {
		return false, err
	}
	return true, nil
}
