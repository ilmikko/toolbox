package temp

const (
	DEFAULT_PATH = "/tmp"
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Default.Path = DEFAULT_PATH
	return cfg
}
