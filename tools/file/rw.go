package file

import (
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"gitlab.com/ilmikko/toolbox/tools/convert"
)

// Cat concatenates contents of small files.
// It is intended for SMALL FILES ONLY. If the file size of any of the input
// files exceeds cfg.MaxCatBytes, it returns an error.
// For larger files, consider using os.Open to avoid buffering.
// If you really want to, you can set cfg.MaxCatBytes to -1.
// You can also use ioutil.ReadAll() but that can hog up your memory.
func (f *File) Cat(files ...string) ([]byte, error) {
	bs := []byte{}
	for _, file := range files {
		of, err := os.Open(file)
		if err != nil {
			return nil, err
		}
		r := &io.LimitedReader{R: of, N: int64(f.cfg.MaxCatBytes + 1)}
		b, err := ioutil.ReadAll(r)
		if err != nil {
			return nil, err
		}
		if r.N <= 0 {
			return nil, fmt.Errorf("Input file %q too large (>%s).",
				file, convert.Bytes2String(f.cfg.MaxCatBytes))
		}
		bs = append(bs, b...)
	}
	return bs, nil
}

func (f *File) CatString(files ...string) (string, error) {
	b, err := f.Cat(files...)
	if err != nil {
		return "", err
	}
	return string(b), nil
}
