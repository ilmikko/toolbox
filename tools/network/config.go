package network

import "fmt"

const (
	DEFAULT_RESOLV_CONF_PATH = "/etc/resolv.conf"
)

// Config holds the configuration for a Network.
type Config struct {
	ResolvconfPath string
}

// Configure configures a Network with a Config.
func (n *Network) Configure(cfg *Config) error {
	n.cfg = cfg
	n.Command = NewCommand(cfg)
	return nil
}

// NewConfig creates a new default configuration.
func NewConfig() *Config {
	cfg := &Config{}
	cfg.ResolvconfPath = DEFAULT_RESOLV_CONF_PATH
	return cfg
}

// NewDefault creates a new Network with default configuration.
// It panics if the default configuration fails with an error.
func NewDefault() *Network {
	c, err := Create(NewConfig())
	if err != nil {
		panic(fmt.Errorf("package network: Default module had an error: %v", err))
	}
	return c
}
