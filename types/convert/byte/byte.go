package byte

import (
	"fmt"
	"math"
	"strconv"
)

type Magnitude int64

var (
	Ln1000 = math.Log(float64(1000))
)

const (
	BYTE Magnitude = iota
	KILOBYTE
	KIBIBYTE
	MEGABYTE
	MEBIBYTE
	GIGABYTE
	GIBIBYTE
	TERABYTE
	TEBIBYTE
	PETABYTE
	PEBIBYTE
	EXABYTE
	EXBIBYTE
)

func byteValue(m Magnitude) int {
	switch m {
	case BYTE:
		return 1
	case KILOBYTE:
		return 1000
	case KIBIBYTE:
		return 1024
	case MEGABYTE:
		return 1000000
	case MEBIBYTE:
		return 1048576
	case GIGABYTE:
		return 1000000000
	case GIBIBYTE:
		return 1073741824
	case TERABYTE:
		return 1000000000000
	case TEBIBYTE:
		return 1099511627776
	case EXABYTE:
		return 1000000000000000
	case EXBIBYTE:
		return 1125899906842624
	default:
		panic(fmt.Sprintf("BUG: No byte value for magnitude: %q", m))
	}
}

func ToBytes(n int, m Magnitude) int {
	return n * byteValue(m)
}

func ToBytesFloat(f float64, m Magnitude) float64 {
	return f * float64(byteValue(m))
}

func ToMagnitude(b int, m Magnitude) int {
	return b / byteValue(m)
}

func ToMagnitudeFloat(f float64, m Magnitude) float64 {
	return f / float64(byteValue(m))
}

// ToString converts bytes of certain magnitude to string.
func ToString(n int, m Magnitude) string {
	f := ToMagnitudeFloat(float64(n), m)
	return strconv.FormatFloat(f, 'f', -1, 64) + " " + m.String()
}

// ToStringBytes finds the best Magnitude for the given bytes.
func ToStringBytes(n int) string {
	log := math.Log(float64(n)) * (1 / Ln1000)
	switch {
	case log < 1:
		return ToString(n, BYTE)
	case log < 2:
		return ToString(n, KILOBYTE)
	case log < 3:
		return ToString(n, MEGABYTE)
	case log < 4:
		return ToString(n, GIGABYTE)
	case log < 5:
		return ToString(n, TERABYTE)
	case log < 6:
		return ToString(n, EXABYTE)
	default:
		return ToString(n, EXABYTE)
	}
}
