package batteries

import "regexp"

const (
	DEFAULT_CLASS_PATH = "/sys/class/power_supply"
)

var (
	DEFAULT_CLASS_REGEX = regexp.MustCompile(`BAT\d+`)
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Class.Path = DEFAULT_CLASS_PATH
	cfg.Class.Regex = DEFAULT_CLASS_REGEX
	return cfg
}
