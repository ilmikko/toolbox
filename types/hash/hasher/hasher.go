package hasher

type Hasher interface {
	HashFile(string) (string, error)
	HashString(string) (string, error)
}
