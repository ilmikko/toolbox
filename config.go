package toolbox

const (
	DEFAULT_ROOT = "/"
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Root = DEFAULT_ROOT
	return cfg
}
