package hasher

import (
	"gitlab.com/ilmikko/toolbox/tools/hash/md5"
	"gitlab.com/ilmikko/toolbox/tools/hash/sha1"
	"gitlab.com/ilmikko/toolbox/tools/hash/sha224"
	"gitlab.com/ilmikko/toolbox/tools/hash/sha256"
	"gitlab.com/ilmikko/toolbox/tools/hash/sha384"
	"gitlab.com/ilmikko/toolbox/tools/hash/sha512"
	"gitlab.com/ilmikko/toolbox/types/hash/algorithm"
)

var (
	Defaults = map[algorithm.Algorithm]Hasher{
		algorithm.MD5:    md5.New(),
		algorithm.SHA1:   sha1.New(),
		algorithm.SHA224: sha224.New(),
		algorithm.SHA256: sha256.New(),
		algorithm.SHA384: sha384.New(),
		algorithm.SHA512: sha512.New(),
	}
)
