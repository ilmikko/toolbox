package sha1

type SHA1 struct {
	cmd *Command
}

func (h *SHA1) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *SHA1) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *SHA1 {
	h := &SHA1{}
	h.cmd = NewCommand()
	return h
}
