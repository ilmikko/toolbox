package file

import "fmt"

const (
	DEFAULT_MAX_CAT_BYTES = 1e6
)

// Config holds the configuration for File.
type Config struct {
	// Maximum bytes that file.Cat can read before returning an error.
	// -1 to disable.
	MaxCatBytes int
}

// Configure configures a File with a Config.
func (f *File) Configure(cfg *Config) error {
	f.cfg = cfg
	return nil
}

// NewConfig creates a new default Config.
func NewConfig() *Config {
	cfg := &Config{}
	cfg.MaxCatBytes = DEFAULT_MAX_CAT_BYTES
	return cfg
}

// NewDefault creates a new File with default configuration.
// It panics if the default configuration fails with an error.
func NewDefault() *File {
	c, err := Create()
	if err != nil {
		panic(fmt.Errorf("package file: Default module had an error: %v", err))
	}
	return c
}
