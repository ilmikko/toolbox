package compressor

import (
	"gitlab.com/ilmikko/toolbox/tools/archive/gzip"
	"gitlab.com/ilmikko/toolbox/tools/archive/tar"
	"gitlab.com/ilmikko/toolbox/types/archive/compression"
)

var (
	Defaults = map[compression.Compression]Compressor{
		compression.GZIP: gzip.New(),
		compression.TAR:  tar.New(),
	}
)
