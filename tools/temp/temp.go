package temp

import (
	"math/rand"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	RANDOM_LENGTH = 22
	TMP_PERM_FILE = 0600
)

var (
	randgen *rand.Rand
)

type Config struct {
	Default struct {
		Path string
	}
}

type Temp struct {
	cfg *Config
}

func (t *Temp) Create(files ...string) ([]string, error) {
	fs := []string{}
	for _, file := range files {
		path, err := t.makeRandom(file)
		if err != nil {
			return fs, err
		}
		fs = append(fs, path)
	}
	return fs, nil
}

func (t *Temp) New() string {
	p, _ := t.makeRandom("")
	return p
}

func (t *Temp) makeRandom(name string) (string, error) {
	if name != "" {
		name = "." + name
	}
	name = t.random() + name
	path := filepath.Join(t.cfg.Default.Path, name)
	f, err := os.OpenFile(path, os.O_CREATE, TMP_PERM_FILE)
	if err != nil {
		return path, err
	}
	if err := f.Close(); err != nil {
		return path, err
	}
	return path, nil
}

func (t *Temp) random() string {
	d := strconv.Itoa(int(time.Now().Unix()))
	r := strconv.Itoa(randgen.Int())

	return (d + r)[:RANDOM_LENGTH]
}

func New() *Temp {
	t := &Temp{}
	t.cfg = NewConfig()
	return t
}

func init() {
	randgen = rand.New(rand.NewSource(time.Now().UnixNano()))
}
