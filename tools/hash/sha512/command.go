package sha512

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	SHA512RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	sha512sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.sha512sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA512RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.sha512sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA512RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.sha512sum = multicommand.New()
	{
		sha512sum := command.New("sha512sum", "--binary")
		c.sha512sum.Commands = append(c.sha512sum.Commands, sha512sum)
	}
	return c
}
