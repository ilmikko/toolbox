package number_test

import (
	"testing"

	"gitlab.com/ilmikko/toolbox/types/convert/number"
)

func TestFloat2String(t *testing.T) {
	testCases := []struct {
		num  float64
		want string
	}{
		{
			num:  1.0,
			want: "1",
		},
		{
			num:  -1.1,
			want: "-1.1",
		},
		{
			num:  42.1,
			want: "42.1",
		},
		{
			num:  81.00004289,
			want: "81.00004289",
		},
	}

	for _, tc := range testCases {
		got := number.Float2String(tc.num)
		want := tc.want
		if got != want {
			t.Errorf("String not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestInt2String(t *testing.T) {
	testCases := []struct {
		num  int
		want string
	}{
		{
			num:  1,
			want: "1",
		},
		{
			num:  -1,
			want: "-1",
		},
		{
			num:  42,
			want: "42",
		},
	}

	for _, tc := range testCases {
		got := number.Int2String(tc.num)
		want := tc.want
		if got != want {
			t.Errorf("String not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestString2Int(t *testing.T) {
	testCases := []struct {
		str  string
		want int
	}{
		{
			str:  "1",
			want: 1,
		},
		{
			str:  "100",
			want: 100,
		},
		{
			str:  "-100",
			want: -100,
		},
		{
			str:  "-10.0",
			want: -10,
		},
		{
			str:  "a1234",
			want: 1234,
		},
		{
			str:  "q 10 00",
			want: 10,
		},
	}

	for _, tc := range testCases {
		got, _ := number.String2Int(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("Int not as expected: \n got: %d\nwant: %d", got, want)
		}
	}
}

func TestString2Float(t *testing.T) {
	testCases := []struct {
		str  string
		want float64
	}{
		{
			str:  "1",
			want: 1,
		},
		{
			str:  "100",
			want: 100.0,
		},
		{
			str:  "-100.0",
			want: -100.0,
		},
		{
			str:  "-10.01",
			want: -10.01,
		},
		{
			str:  "a12.34",
			want: 12.34,
		},
		{
			str:  "q-1000.421",
			want: -1000.421,
		},
	}

	for _, tc := range testCases {
		got, _ := number.String2Float(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("Float not as expected: \n got: %f\nwant: %f", got, want)
		}
	}
}
