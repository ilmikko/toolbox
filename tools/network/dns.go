package network

import "gitlab.com/ilmikko/toolbox/types/network/nameserver"

// Nameservers returns all DNS nameservers.
func (n *Network) Nameservers() ([]*nameserver.Nameserver, error) {
	nss, err := n.Command.Nameservers()
	if err != nil {
		return nil, err
	}
	return nss, nil
}
