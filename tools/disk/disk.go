package disk

import (
	"gitlab.com/ilmikko/toolbox/tools/disk/capacity"
	"gitlab.com/ilmikko/toolbox/tools/disk/partition"
	"gitlab.com/ilmikko/toolbox/types/convert/byte"
)

type Config struct {
	Default struct {
		Magnitude byte.Magnitude
	}
}

type Disk struct {
	cfg       *Config
	Capacity  *capacity.Capacity
	Partition *partition.Partition
}

func New() *Disk {
	a := &Disk{}
	a.cfg = NewConfig()
	a.Capacity = capacity.New()
	a.Partition = partition.New()
	return a
}
