package sha256

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	SHA256RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	sha256sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.sha256sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA256RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.sha256sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA256RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.sha256sum = multicommand.New()
	{
		sha256sum := command.New("sha256sum", "--binary")
		c.sha256sum.Commands = append(c.sha256sum.Commands, sha256sum)
	}
	return c
}
