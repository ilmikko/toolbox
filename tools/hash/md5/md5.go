package md5

type MD5 struct {
	cmd *Command
}

func (h *MD5) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *MD5) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *MD5 {
	h := &MD5{}
	h.cmd = NewCommand()
	return h
}
