package network

type Network struct {
	cfg *Config

	Command *Command
}

func Create(cfg *Config) (*Network, error) {
	n := New()
	if err := n.Configure(cfg); err != nil {
		return nil, err
	}
	return n, nil
}

func New() *Network {
	n := &Network{}
	return n
}
