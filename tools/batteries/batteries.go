package batteries

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/tools/convert"
	"gitlab.com/ilmikko/toolbox/tools/file"
)

type Config struct {
	Class struct {
		Path  string
		Regex *regexp.Regexp
	}
}

type Batteries struct {
	CFG *Config
}

func (b *Batteries) Configure(cfg *Config) error {
	b.CFG = cfg
	return nil
}

func (b *Batteries) List() ([]string, error) {
	paths, err := file.List(b.CFG.Class.Path)
	if err != nil {
		return nil, err
	}
	batteries := []string{}
	for _, path := range paths {
		if !b.CFG.Class.Regex.MatchString(path) {
			continue
		}
		batteries = append(batteries, path)
	}
	return batteries, nil
}

// Percentage returns all battery percentages in the range 0..1.
func (b *Batteries) Percentage(list ...string) ([]float64, error) {
	percs := []float64{}
	for _, path := range list {
		perc, err := b.PercentageFor(path)
		if err != nil {
			return nil, err
		}
		percs = append(percs, perc)
	}

	return percs, nil
}

// PercentageFor returns a single battery's percentage in the range 0..1.
func (b *Batteries) PercentageFor(path string) (float64, error) {
	path = file.JoinPathRelative(b.CFG.Class.Path, path, "/capacity")
	str, err := file.CatString(path)
	if err != nil {
		return -1, err
	}
	f, err := convert.String2Float(str)
	if err != nil {
		return -1, err
	}
	return f / 100.0, nil
}

func (b *Batteries) Status(list ...string) ([]string, error) {
	stats := []string{}
	for _, path := range list {
		stat, err := b.StatusFor(path)
		if err != nil {
			return nil, err
		}
		stats = append(stats, stat)
	}

	return stats, nil
}

func (b *Batteries) StatusFor(path string) (string, error) {
	path = file.JoinPathRelative(b.CFG.Class.Path, path, "/status")
	str, err := file.CatString(path)
	if err != nil {
		return "", err
	}
	return strings.TrimSuffix(str, "\n"), nil
}

func Create() (*Batteries, error) {
	b := New()
	if err := b.Configure(NewConfig()); err != nil {
		return nil, err
	}
	return b, nil
}

func New() *Batteries {
	return &Batteries{}
}
