package sha384

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	SHA384RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	sha384sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.sha384sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA384RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.sha384sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA384RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.sha384sum = multicommand.New()
	{
		sha384sum := command.New("sha384sum", "--binary")
		c.sha384sum.Commands = append(c.sha384sum.Commands, sha384sum)
	}
	return c
}
