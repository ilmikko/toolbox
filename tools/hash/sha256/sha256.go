package sha256

type SHA256 struct {
	cmd *Command
}

func (h *SHA256) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *SHA256) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *SHA256 {
	h := &SHA256{}
	h.cmd = NewCommand()
	return h
}
