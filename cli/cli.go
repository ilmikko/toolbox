package cli

type Cli struct {
	cfg *Config
}

func Create() (*Cli, error) {
	c := New()
	if err := c.Configure(NewConfig()); err != nil {
		return nil, err
	}
	return c, nil
}

func New() *Cli {
	return &Cli{}
}
