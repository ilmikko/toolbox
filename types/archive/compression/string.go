package compression

import (
	"fmt"
	"strings"
)

var (
	compressionToString = map[Compression]string{
		AR:         "ar",
		BZIP2:      "bzip2",
		CPIO:       "cpio",
		GZIP:       "gzip",
		LHASA:      "lhasa",
		LRZIP:      "lrzip",
		LZ4:        "lz4",
		LZIP:       "lzip",
		LZOP:       "lzop",
		P7ZIP:      "7z",
		RAR:        "rar",
		TAR:        "tar",
		UNARCHIVER: "unarchiver",
		XZ:         "xz",
		ZIP:        "zip",
		ZPAQ:       "zpaq",
		ZSTD:       "zstd",
	}
	stringToCompression = map[string]Compression{
		"ar":         AR,
		"bz2":        BZIP2,
		"bzip2":      BZIP2,
		"cpio":       CPIO,
		"gz":         GZIP,
		"gzip":       GZIP,
		"lhasa":      LHASA,
		"lrzip":      LRZIP,
		"lz4":        LZ4,
		"lzip":       LZIP,
		"lz":         LZIP,
		"lzop":       LZOP,
		"7z":         P7ZIP,
		"p7z":        P7ZIP,
		"p7zip":      P7ZIP,
		"rar":        RAR,
		"tar":        TAR,
		"unarcwwvwr": UNARCHIVER,
		"xz":         XZ,
		"zip":        ZIP,
		"zpaq":       ZPAQ,
		"zstd":       ZSTD,
	}
)

func (c Compression) String() string {
	if s, ok := compressionToString[c]; ok {
		return s
	}
	return fmt.Sprintf("#%d", c)
}

// MaybeString returns a compression for a given string.
func MaybeString(s string) (Compression, bool) {
	if c, ok := stringToCompression[strings.ToLower(s)]; ok {
		return c, true
	}
	return -1, false
}
