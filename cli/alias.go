package cli

import (
	"fmt"

	"gitlab.com/ilmikko/toolbox/types/convert/number"
)

const (
	INVALID_ARGUMENT     = "Invalid argument:"
	NEEDS_MORE_ARGUMENTS = "Needs more arguments:"
	NO_ARGUMENTS         = "No arguments specified."
	ALIAS_STACK          = 50
)

func (c *Cli) Alias(args []string) ([]string, error) {
	if len(args) == 0 {
		fmt.Println(c.Usage())
		return nil, fmt.Errorf(NO_ARGUMENTS)
	}
	for i := 0; i < ALIAS_STACK; i++ {
		if len(args) == 0 {
			break
		}
		if i == ALIAS_STACK-1 {
			return nil, fmt.Errorf("Bug: Alias loop did not break within stack size %d.", ALIAS_STACK)
		}
		switch a := args[0]; a {
		case "battery":
			// battery -> batteries 0
			args = append([]string{"batteries"}, args[1:]...)
			if len(args) <= 1 {
				args = append(args, "0")
			}
			// battery 2 -> batteries 2
			if _, ok := number.MaybeInt(args[1]); !ok {
				if args[1] != "*" {
					args = append([]string{args[0], "0"}, args[1:]...)
				}
			}
			continue
		case "batteries":
			// batteries -> batteries *
			if len(args) <= 1 {
				args = append(args, "*")
			}
			// batteries list -> batteries * list
			if _, ok := number.MaybeInt(args[1]); !ok {
				if args[1] != "*" {
					args = append([]string{args[0], "*"}, args[1:]...)
				}
			}
			// batteries * -> batteries * list
			if len(args) <= 2 {
				args = append(args, "list")
			}
			switch a := args[2]; a {
			case "%", "perc":
				// batteries * % -> batteries * percentage
				args[2] = "percentage"
				continue
			case "stat":
				// batteries * stat -> batteries * status
				args[2] = "status"
				continue
			}
		case "convert":
			if len(args) <= 1 {
				return nil, fmt.Errorf("%s %s", NEEDS_MORE_ARGUMENTS, args)
			}
			switch a := args[1]; a {
			case "%", "perc":
				// convert % -> convert percentage
				args[1] = "percentage"
				continue
			case "int":
				// convert int -> convert integer
				args[1] = "integer"
				continue
			}
		case "disk":
			if len(args) <= 1 {
				return nil, fmt.Errorf("%s %s", NEEDS_MORE_ARGUMENTS, args)
			}
			switch a := args[1]; a {
			case "size":
				// disk size -> disk capacity
				args[1] = "capacity"
				continue
			case "part":
				// disk part -> disk partition
				args[1] = "partition"
				continue
			case "capacity", "partition":
				if len(args) <= 2 {
					return nil, fmt.Errorf("%s %s", NEEDS_MORE_ARGUMENTS, args)
				}
			}
		case "net":
			// net -> network
			args[0] = "network"
			continue
		case "network":
			if len(args) <= 1 {
				return nil, fmt.Errorf("%s %s", NEEDS_MORE_ARGUMENTS, args)
			}
			switch a := args[1]; a {
			case "addr":
				// network addr -> network address
				args[1] = "address"
				continue
			case "conn":
				// network conn -> network connection
				args[1] = "connection"
				continue
			case "connection":
				if len(args) <= 2 {
					return nil, fmt.Errorf("%s %s", NEEDS_MORE_ARGUMENTS, args)
				}
			case "dns", "nameserver":
				// network dns -> network nameservers
				args[1] = "nameservers"
				continue
			}
		case "tmp":
			// tmp -> temp
			args = append([]string{"temp"}, args[1:]...)
			continue
		}
		break
	}
	return args, nil
}
