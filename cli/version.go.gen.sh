#!/usr/bin/env bash
# Git version go code generator.

ME="$(realpath $0)";
MAJOR="$(git tag)";
MINOR="$(git rev-list HEAD --count)";
[ -n "$MAJOR" ] || MAJOR="v0.0";
REVISION="$(git rev-parse HEAD | head -c8)";
VERSION="$MAJOR.$MINOR ($REVISION)";

GO="$(dirname "$ME")/$(basename $ME .gen.sh)";
PKG="$(basename "$(dirname "$ME")")";

cat >$GO <<GO
// DO NOT TOUCH //
// Generated from git repo by $0 //
package $PKG

import "fmt"

const (
	VERSION = "$VERSION"
)

func (c *Cli) Version() string {
	return fmt.Sprintf("Version %s", VERSION)
}
GO
