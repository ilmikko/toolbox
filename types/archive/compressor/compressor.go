package compressor

type Compressor interface {
	Compress(file string) (string, error)
	CompressAll(files ...string) ([]string, error)
}
