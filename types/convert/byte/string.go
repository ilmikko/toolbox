package byte

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

var (
	MagnitudeStringRX      = regexp.MustCompile(`^([0-9]*)([A-Z]*)$`)
	MagnitudeStringFloatRX = regexp.MustCompile(`^([0-9]*(\.[0-9]*)?)([A-Z]*)$`)
)

var (
	magnitudeToString = map[Magnitude]string{
		BYTE:     "B",
		KILOBYTE: "KB",
		KIBIBYTE: "KiB",
		MEGABYTE: "MB",
		MEBIBYTE: "MiB",
		GIGABYTE: "GB",
		GIBIBYTE: "GiB",
		TERABYTE: "TB",
		TEBIBYTE: "TiB",
		PETABYTE: "PB",
		PEBIBYTE: "PiB",
		EXABYTE:  "EB",
		EXBIBYTE: "EiB",
	}
	stringToMagnitude = map[string]Magnitude{
		"B":   BYTE,
		"K":   KIBIBYTE,
		"KB":  KILOBYTE,
		"KIB": KIBIBYTE,
		"M":   MEBIBYTE,
		"MB":  MEGABYTE,
		"MIB": MEBIBYTE,
		"G":   GIBIBYTE,
		"GB":  GIGABYTE,
		"GIB": GIBIBYTE,
		"T":   TEBIBYTE,
		"TB":  TERABYTE,
		"TIB": TEBIBYTE,
		"P":   PEBIBYTE,
		"PB":  PETABYTE,
		"PIB": PEBIBYTE,
		"E":   EXBIBYTE,
		"EB":  EXABYTE,
		"EIB": EXBIBYTE,
	}
)

// Parse parses a string containing a valid magnitude string and optionally an int value.
func Parse(s string) (int, Magnitude, error) {
	s = strings.ToUpper(s)
	ms := MagnitudeStringRX.FindStringSubmatch(s)

	if len(ms) < 3 {
		return -1, -1, fmt.Errorf("Format error: %q does not conform to %q", s, MagnitudeStringRX)
	}

	n := 1
	if ms[1] != "" {
		an, err := strconv.Atoi(ms[1])
		if err != nil {
			return -1, -1, err
		}
		n = an
	}

	m := BYTE
	if mag := ms[2]; mag != "" {
		am, err := ParseMagnitude(mag)
		if err != nil {
			return -1, -1, err
		}
		m = am
	}

	return n, m, nil
}

// ParseFloat parses a string containing a valid magnitude string and optionally a float value.
func ParseFloat(s string) (float64, Magnitude, error) {
	s = strings.ToUpper(s)
	ms := MagnitudeStringFloatRX.FindStringSubmatch(s)

	if len(ms) < 3 {
		return -1, -1, fmt.Errorf("Format error: %q does not conform to %q", s, MagnitudeStringFloatRX)
	}

	n := 1.0
	if ms[1] != "" {
		an, err := strconv.ParseFloat(ms[1], 64)
		if err != nil {
			return -1, -1, err
		}
		n = an
	}

	m := BYTE
	if mag := ms[3]; mag != "" {
		am, err := ParseMagnitude(mag)
		if err != nil {
			return -1, -1, err
		}
		m = am
	}

	return n, m, nil
}

func ParseMagnitude(s string) (Magnitude, error) {
	m, ok := stringToMagnitude[s]
	if !ok {
		return -1, fmt.Errorf("Cannot find magnitude for %q", s)
	}
	return m, nil
}

func (m Magnitude) String() string {
	if s, ok := magnitudeToString[m]; ok {
		return s
	}
	return fmt.Sprintf("#%d", m)
}
