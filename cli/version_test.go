package cli_test

import (
	"regexp"
	"testing"

	"gitlab.com/ilmikko/toolbox/cli"
)

var (
	VersionRX = regexp.MustCompile(`Version [0-9]+\.[0-9]+\.[0-9]+ \([0-9a-f]+\)`)
)

func TestVersion(t *testing.T) {
	cli := cli.New()
	got := cli.Version()

	if VersionRX.MatchString(got) {
		t.Errorf("Version not as expected: \n got: %q\nwant: %q", got, VersionRX)
	}
}
