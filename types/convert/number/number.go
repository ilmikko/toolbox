package number

import (
	"fmt"
	"regexp"
	"strconv"
)

var (
	IntRX   = regexp.MustCompile(`-?[0-9]+`)
	FloatRX = regexp.MustCompile(`-?[0-9]+(\.[0-9]+)?`)
)

// Float2String converts any float to its human readable string.
func Float2String(f float64) string {
	return strconv.FormatFloat(f, 'f', -1, 64)
}

// Int2String converts any int to its human readable string.
func Int2String(n int) string {
	return strconv.Itoa(n)
}

// MaybeInt returns an integer for a given string if successful.
func MaybeInt(s string) (int, bool) {
	if c, err := String2Int(s); err == nil {
		return c, true
	}
	return 0, false
}

// String2Float converts any string to a float - only the first sequence of numbers is regarded.
// For example:
// String2Float("2.5 pretzels") -> 2.5
// String2Float("ba-9.4.xz") -> -9.4
// String2Float("abcde") -> 0.0
func String2Float(s string) (float64, error) {
	ns := FloatRX.FindString(s)
	if ns == "" {
		return 0, fmt.Errorf("No float found in string.")
	}
	f, err := strconv.ParseFloat(ns, 64)
	if err != nil {
		return 0, err
	}
	return f, nil
}

// String2Int converts any string to an integer - only the first sequence of numbers is regarded.
// For example:
// String2Int("200 apples") -> 200
// String2Int("rx-800z") -> -800
// String2Int("abcde") -> 0
func String2Int(s string) (int, error) {
	ns := IntRX.FindString(s)
	if ns == "" {
		return 0, fmt.Errorf("No integer found in string.")
	}
	n, err := strconv.Atoi(ns)
	if err != nil {
		return 0, err
	}
	return n, nil
}
