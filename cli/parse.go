package cli

import (
	"fmt"
	"log"
	"strings"

	"gitlab.com/ilmikko/toolbox"
	"gitlab.com/ilmikko/toolbox/types/archive/compression"
	"gitlab.com/ilmikko/toolbox/types/convert/number"
	"gitlab.com/ilmikko/toolbox/types/hash/algorithm"
)

func (c *Cli) Parse(t *toolbox.Toolbox, args []string) error {
	args, err := c.Alias(args)
	if err != nil {
		return err
	}
	switch a := args[0]; a {
	case "batteries":
		args = args[1:]
		var ss []string
		var err error

		list, err := t.Batteries.List()
		if err != nil {
			return err
		}

		if args[0] != "*" {
			i, err := number.String2Int(args[0])
			if err != nil {
				return err
			}
			if i > len(list)-1 {
				return fmt.Errorf("Battery %d not in batteries: %s.", i, list)
			}
			list = []string{list[i]}
		}
		args = args[1:]

		switch a := args[0]; a {
		case "list":
			percs, err := t.Batteries.Percentage(list...)
			if err != nil {
				return err
			}
			stats, err := t.Batteries.Status(list...)
			if err != nil {
				return err
			}
			for i := range percs {
				ss = append(ss, fmt.Sprintf("%d%% (%s)", int(percs[i]*100), stats[i]))
			}
		case "percentage":
			var ps []float64
			ps, err = t.Batteries.Percentage(list...)
			for _, p := range ps {
				ss = append(ss, fmt.Sprintf("%d%%", int(p*100)))
			}
		case "status":
			ss, err = t.Batteries.Status(list...)
		default:
			return fmt.Errorf("Unknown arg: %q", args[0])
		}

		for _, s := range ss {
			fmt.Println(s)
		}
		if err != nil {
			return err
		}
	case "checksum":
		var hs []string
		var err error

		if a, ok := algorithm.MaybeString(args[1]); ok {
			hs, err = t.Hash.ChecksumWith(a, args[2:]...)
		} else {
			hs, err = t.Hash.Checksum(args[1:]...)
		}

		for _, h := range hs {
			fmt.Println(h)
		}
		if err != nil {
			return err
		}
	case "compress":
		var fs []string
		var err error

		if c, ok := compression.MaybeString(args[1]); ok {
			fs, err = t.Archive.CompressWith(c, args[2:]...)
		} else {
			fs, err = t.Archive.Compress(args[1:]...)
		}

		for _, f := range fs {
			fmt.Println(f)
		}
		if err != nil {
			return err
		}
	case "convert":
		switch a := args[1]; a {
		case "bytes":
			from := args[2]
			to := "B"
			if len(args) > 3 {
				to = args[3]
			}
			r, err := t.Convert.BytesStringFloat(from, to)
			if err != nil {
				return err
			}
			fmt.Println(t.Convert.Float2String(r))
		case "float":
			f, err := t.Convert.String2Float(strings.Join(args[2:], " "))
			if err != nil {
				return err
			}
			fmt.Println(t.Convert.Float2String(f))
		case "integer":
			i, err := t.Convert.String2Int(strings.Join(args[2:], " "))
			if err != nil {
				return err
			}
			fmt.Println(t.Convert.Int2String(i))
		case "percentage":
			perc, err := t.Convert.String2Percentage(args[2])
			if err != nil {
				return err
			}
			from := strings.Join(args[3:], " ")
			p, err := perc.ApplyString(from)
			if err != nil {
				return err
			}
			fmt.Println(p)
		default:
			return fmt.Errorf("Unknown conversion: %q", args[1])
		}
	case "disk":
		switch a := args[1]; a {
		case "capacity":
			disk := args[2]
			b, err := t.Disk.Capacity.Bytes(disk)
			if err != nil {
				return err
			}
			fmt.Println(t.Convert.Int2String(b))
		case "partition":
			disk := args[2]
			ps, err := t.Disk.Partition.Run(disk, args[3:]...)
			for _, p := range ps {
				fmt.Println(p)
			}
			if err != nil {
				return err
			}
		default:
			return fmt.Errorf("Unknown disk operation: %q", args[1])
		}
	case "hash":
		var hs string
		var err error

		if a, ok := algorithm.MaybeString(args[1]); ok {
			hs, err = t.Hash.StringWith(a, strings.Join(args[2:], " "))
		} else {
			hs, err = t.Hash.String(strings.Join(args[1:], " "))
		}

		if err != nil {
			return err
		}
		fmt.Println(hs)
	case "install":
		if err := t.Packman.Install(args[1:]...); err != nil {
			return err
		}
		log.Println("OK")
	case "network":
		switch a := args[1]; a {
		case "address":
			addrs, err := t.Network.Addresses()
			if err != nil {
				return err
			}
			for _, addr := range addrs {
				fmt.Println(addr)
			}
		case "connection":
			for _, host := range args[2:] {
				if ok, err := t.Network.Connection(host); ok {
					fmt.Printf("%s %s\n", host, "OK")
				} else {
					fmt.Printf("%s %s\n", host, "FAIL")
					return err
				}
			}
		case "nameservers":
			nss, err := t.Network.Nameservers()
			if err != nil {
				return err
			}
			for _, ns := range nss {
				fmt.Println(ns)
			}
		default:
			return fmt.Errorf("Unknown network operation: %q", args[1])
		}
	case "temp":
		ts, err := t.Temp.Create(args[1:]...)
		for _, t := range ts {
			fmt.Println(t)
		}
		if err != nil {
			return err
		}
	case "usage":
		fmt.Println(c.Usage())
	case "version":
		fmt.Println(c.Version())
	default:
		fmt.Println(c.Usage())
		return fmt.Errorf("Unknown tool: %q", a)
	}
	return nil
}
