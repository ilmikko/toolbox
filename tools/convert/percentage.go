package convert

import (
	"gitlab.com/ilmikko/toolbox/types/convert/percentage"
)

func (c *Convert) Float2Percentage(f float64) percentage.Percentage {
	return percentage.Float2Percentage(f)
}

func (c *Convert) Percentage2Float(p percentage.Percentage) float64 {
	return percentage.Percentage2Float(p)
}

func (c *Convert) Percentage2String(p percentage.Percentage) string {
	return percentage.Percentage2String(p)
}

func (c *Convert) String2Percentage(s string) (percentage.Percentage, error) {
	return percentage.String2Percentage(s)
}
