package tar

type Tar struct {
	cmd *Command
}

// Compress performs a Tar archival on a file or folder.
// Passing multiple files to the command does NOT create an archive with those files;
// Please use a folder if combining the files is required.
func (c *Tar) Compress(file string) (string, error) {
	if err := c.cmd.Compress(file); err != nil {
		return "", err
	}
	return file + ".tar", nil
}

func (c *Tar) CompressAll(files ...string) ([]string, error) {
	fs := []string{}
	for _, file := range files {
		f, err := c.Compress(file)
		if err != nil {
			return fs, err
		}
		fs = append(fs, f)
	}
	return fs, nil
}

func New() *Tar {
	c := &Tar{}
	c.cmd = NewCommand()
	return c
}
