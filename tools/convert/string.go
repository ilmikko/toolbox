package convert

import (
	"gitlab.com/ilmikko/toolbox/types/convert/number"
)

func (c *Convert) Float2String(f float64) string {
	return number.Float2String(f)
}

func (c *Convert) Int2String(n int) string {
	return number.Int2String(n)
}

func (c *Convert) String2Float(s string) (float64, error) {
	return number.String2Float(s)
}

func (c *Convert) String2Int(s string) (int, error) {
	return number.String2Int(s)
}
