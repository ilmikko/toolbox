package nameserver

import "net"

type Nameserver struct {
	Address *net.IP
}

func (ns *Nameserver) String() string {
	return ns.Address.String()
}
