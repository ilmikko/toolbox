package percentage_test

import (
	"testing"

	"gitlab.com/ilmikko/toolbox/types/convert/percentage"
)

func TestApplyInt(t *testing.T) {
	testCases := []struct {
		perc percentage.Percentage
		num  int
		want int
	}{
		{
			perc: percentage.Percentage(0.5),
			num:  100,
			want: 50,
		},
		{
			perc: percentage.Percentage(0.09),
			num:  10,
			want: 1,
		},
		{
			perc: percentage.Percentage(0.05),
			num:  10,
			want: 1,
		},
		{
			perc: percentage.Percentage(0.02),
			num:  10,
			want: 0,
		},
		{
			perc: percentage.Percentage(0),
			num:  10,
			want: 0,
		},
	}

	for _, tc := range testCases {
		got := tc.perc.ApplyInt(tc.num)
		want := tc.want
		if got != want {
			t.Errorf("Int not as expected: \n got: %d\nwant: %d", got, want)
		}
	}
}

func TestApplyString(t *testing.T) {
	testCases := []struct {
		perc percentage.Percentage
		str  string
		want string
	}{
		{
			perc: percentage.Percentage(0.5),
			str:  "100 apples",
			want: "50 apples",
		},
		{
			perc: percentage.Percentage(0.25),
			str:  "I have 100 apples",
			want: "I have 25 apples",
		},
		{
			perc: percentage.Percentage(0.05),
			str:  "I have 10 apples",
			want: "I have 0.5 apples",
		},
		{
			perc: percentage.Percentage(0.12),
			str:  "80 KB",
			want: "9.6 KB",
		},
		{
			perc: percentage.Percentage(2.5),
			str:  "800 €",
			want: "2000 €",
		},
		{
			perc: percentage.Percentage(2.5),
			str:  "CHF9500",
			want: "CHF23750",
		},
	}

	for _, tc := range testCases {
		got, _ := tc.perc.ApplyString(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("String not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestPercentage2Float(t *testing.T) {
	testCases := []struct {
		perc percentage.Percentage
		want float64
	}{
		{
			perc: percentage.Percentage(0.5),
			want: 0.5,
		},
		{
			perc: percentage.Percentage(0.1),
			want: 0.1,
		},
		{
			perc: percentage.Percentage(1.5),
			want: 1.5,
		},
		{
			perc: percentage.Percentage(-0.5),
			want: -0.5,
		},
	}

	for _, tc := range testCases {
		got := percentage.Percentage2Float(tc.perc)
		want := tc.want
		if got != want {
			t.Errorf("Float not as expected: \n got: %f\nwant: %f", got, want)
		}
	}
}

func TestPercentage2String(t *testing.T) {
	testCases := []struct {
		perc percentage.Percentage
		want string
	}{
		{
			perc: percentage.Percentage(0.5),
			want: "50%",
		},
		{
			perc: percentage.Percentage(0.1),
			want: "10%",
		},
		{
			perc: percentage.Percentage(1.5),
			want: "150%",
		},
		{
			perc: percentage.Percentage(0.005),
			want: "0.5%",
		},
		{
			perc: percentage.Percentage(-0.0001),
			want: "-0.01%",
		},
	}

	for _, tc := range testCases {
		got := percentage.Percentage2String(tc.perc)
		want := tc.want
		if got != want {
			t.Errorf("String not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestString2Percentage(t *testing.T) {
	testCases := []struct {
		str  string
		want percentage.Percentage
	}{
		{
			str:  "25%",
			want: percentage.Percentage(0.25),
		},
		{
			str:  "-25%",
			want: percentage.Percentage(-0.25),
		},
		{
			str:  "05%",
			want: percentage.Percentage(0.05),
		},
		{
			str:  "0%",
			want: percentage.Percentage(0.0),
		},
		{
			str:  " 100%",
			want: percentage.Percentage(1.0),
		},
		{
			str:  "90",
			want: percentage.Percentage(0.9),
		},
		{
			str:  "a25 r %",
			want: percentage.Percentage(0.25),
		},
	}

	for _, tc := range testCases {
		got, _ := percentage.String2Percentage(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("Percentage not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
