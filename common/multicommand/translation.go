package multicommand

var (
	NoTranslation = func(ss []string) []string { return ss }
)

type Translation func([]string) []string
