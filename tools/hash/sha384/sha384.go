package sha384

type SHA384 struct {
	cmd *Command
}

func (h *SHA384) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *SHA384) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *SHA384 {
	h := &SHA384{}
	h.cmd = NewCommand()
	return h
}
