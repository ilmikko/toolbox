package partition

import (
	"fmt"
	"log"
	"regexp"
	"strconv"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/tools/convert"
	"gitlab.com/ilmikko/toolbox/tools/disk/capacity"
)

var (
	ONLY_B_RX = regexp.MustCompile(`^(\d+)B$`)
	SFDISK_RX = regexp.MustCompile(`\b(\S+): Created a new partition \d+ .* of size (\d+\.?\d+\s*\S*\w+)\b`)
)

type Partition struct {
	commands []*command.Command
	convert  *convert.Convert
}

// Bytes partitions a disk according to bytes.
// It returns the created partitions as strings.
func (c *Partition) Bytes(disk string, is ...int) ([]string, error) {
	in := command.Input{
		Args: []string{disk},
	}
	for _, i := range is {
		in.Args = append(in.Args, strconv.Itoa(i)+"B")
	}
	for i, cmd := range c.commands {
		out, err := cmd.Run(in)
		if err == nil {
			return strings.Split(out, "\n"), nil
		}
		log.Printf("Partition: Command #%d failed: %v", i, err)
	}
	return nil, fmt.Errorf("No more partition commands :(")
}

// Run partitions a disk based on human readable values.
// You can use values such as 2GiB or 2G for multiples of 1024,
// and values such as 2GB for multiples of 1000.
// You can also use percentages, which will be parsed of the remaining space.
// That is, while "10GiB 100%" would work, "100% 10GiB" would not, as there is no
// remaining space left after using 100% of the space.
// It returns the created partitions as strings.
func (c *Partition) Run(disk string, ps ...string) ([]string, error) {
	byteSizes := []int{}
	diskCapacity := 0

	for _, p := range ps {
		if strings.HasSuffix(p, "%") {
			if diskCapacity == 0 {
				c, err := capacity.New().Bytes(disk)
				if err != nil {
					return nil, err
				}
				diskCapacity = c
			}
			perc, err := c.convert.String2Percentage(p)
			if err != nil {
				return nil, err
			}
			byteSizes = append(byteSizes, perc.ApplyInt(diskCapacity))
			continue
		}

		b, err := c.convert.BytesString(p, "")
		if err != nil {
			return nil, err
		}
		byteSizes = append(byteSizes, b)
	}
	ps, err := c.Bytes(disk, byteSizes...)
	if err != nil {
		return nil, err
	}
	return ps, nil
}

func New() *Partition {
	c := &Partition{}
	c.convert = convert.New()
	{
		sfdisk := command.New("sfdisk")
		sfdisk.InputTranslation = func(in command.Input) command.Input {
			stdin := "label:gpt\n"
			disk, parts := in.Args[0], in.Args[1:]
			for _, p := range parts {
				// sfdisk does not like 100B; it should be 100 instead.
				// if we suffix with only B, we remove the B.
				stdin += "size=" + ONLY_B_RX.ReplaceAllString(p, `$1`) + "\n"
			}
			in.Args = []string{disk}
			in.Stdin = strings.NewReader(stdin)
			return in
		}
		sfdisk.OutputTranslation = func(out string) (string, error) {
			ms := SFDISK_RX.FindAllStringSubmatch(out, -1)
			ps := []string{}
			for _, gs := range ms {
				if len(gs) < 3 {
					return "", fmt.Errorf("Not found from match: %q (match: %q)", SFDISK_RX, gs[0])
				}
				b, err := c.convert.BytesString(gs[2], "B")
				if err != nil {
					return "", fmt.Errorf("Byte parsing failed: %v", err)
				}
				bs := c.convert.Int2String(b)
				ps = append(ps, "/"+gs[1]+" "+bs)
			}
			return strings.Join(ps, "\n"), nil
		}
		c.commands = append(c.commands, sfdisk)
	}
	return c
}
