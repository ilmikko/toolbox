package hash

import (
	"gitlab.com/ilmikko/toolbox/types/hash/algorithm"
)

const (
	DEFAULT_ALGORITHM = algorithm.SHA256
	DEFAULT_CHECKSUM  = algorithm.MD5
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Default.Algorithm = DEFAULT_ALGORITHM
	cfg.Default.Checksum = DEFAULT_CHECKSUM
	return cfg
}
