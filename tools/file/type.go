package file

import (
	"fmt"
	"os"
)

func (f *File) IsDir(path string) bool {
	d, err := os.Stat(path)
	if err != nil {
		return false
	}
	if !d.IsDir() {
		return false
	}
	return true
}

func (f *File) MustDir(dir string) error {
	if !f.IsDir(dir) {
		return fmt.Errorf("Path %q is not a directory", dir)
	}
	return nil
}
