package packman

type Packman struct {
	cmd *Command
}

// Compress performs a Gzip compression on a file.
func (p *Packman) Install(pkgs ...string) error {
	for _, pkg := range pkgs {
		if err := p.cmd.Install(pkg); err != nil {
			return err
		}
	}
	return nil
}

func New() *Packman {
	c := &Packman{}
	c.cmd = NewCommand()
	return c
}
