package tar

import (
	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
)

type Command struct {
	compress *multicommand.MultiCommand
}

func (c *Command) Compress(file string) error {
	if _, err := c.compress.RunArgs(file); err != nil {
		return err
	}
	return nil
}

func NewCommand() *Command {
	c := &Command{}
	c.compress = multicommand.New()
	{
		tar := command.New("tar", "--create")
		tar.InputTranslation = func(in command.Input) command.Input {
			in.Args = append([]string{"--file", in.Args[0] + ".tar"}, in.Args...)
			return in
		}
		c.compress.Commands = append(c.compress.Commands, tar)
	}
	return c
}
