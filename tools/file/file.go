package file

type File struct {
	cfg *Config
}

func Create() (*File, error) {
	f := New()
	if err := f.Configure(NewConfig()); err != nil {
		return nil, err
	}
	return f, nil
}

func New() *File {
	return &File{}
}
