package convert

type Convert struct{}

func Create() (*Convert, error) {
	c := New()
	if err := c.Configure(NewConfig()); err != nil {
		return nil, err
	}
	return c, nil
}

func New() *Convert {
	c := &Convert{}
	return c
}
