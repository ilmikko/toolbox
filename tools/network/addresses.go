package network

import (
	"gitlab.com/ilmikko/toolbox/types/network/netinterface"
)

func (n *Network) Addresses() ([]*netinterface.NetInterface, error) {
	addrs, err := n.Command.Address()
	if err != nil {
		return nil, err
	}
	return addrs, nil
}
