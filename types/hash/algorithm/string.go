package algorithm

import (
	"fmt"
	"strings"
)

var (
	algorithmToString = map[Algorithm]string{
		ADLER32: "Adler-32",
		CRC32:   "CRC-32",
		CRC64:   "CRC-64",
		FNV:     "FNV-1",
		MD5:     "MD5",
		SHA1:    "SHA1",
		SHA224:  "SHA224",
		SHA256:  "SHA256",
		SHA384:  "SHA384",
		SHA512:  "SHA512",
	}
	stringToAlgorithm = map[string]Algorithm{
		"adler32": ADLER32,
		"crc":     CRC64,
		"crc32":   CRC32,
		"crc64":   CRC64,
		"fnv":     FNV,
		"fnv1":    FNV,
		"md5":     MD5,
		"sha":     SHA1,
		"sha1":    SHA1,
		"sha224":  SHA224,
		"sha256":  SHA256,
		"sha384":  SHA384,
		"sha512":  SHA512,
	}
)

func (a Algorithm) String() string {
	if s, ok := algorithmToString[a]; ok {
		return s
	}
	return fmt.Sprintf("#%d", a)
}

// MaybeString returns an algorithm for a given string.
func MaybeString(s string) (Algorithm, bool) {
	s = strings.ToLower(s)
	s = strings.ReplaceAll(s, "-", "")
	if a, ok := stringToAlgorithm[s]; ok {
		return a, true
	}
	return -1, false
}
