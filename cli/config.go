package cli

import (
	"fmt"
	"os"
)

var (
	DEFAULT_PATH = os.Args[0]
)

// Config holds the configuration for a Cli.
type Config struct {
	// Path this CLI points to, usually $0.
	Path string
}

// Configure configures a Cli with a Config.
func (c *Cli) Configure(cfg *Config) error {
	c.cfg = cfg
	return nil
}

// NewConfig creates a new default configuration.
func NewConfig() *Config {
	cfg := &Config{}
	cfg.Path = DEFAULT_PATH
	return cfg
}

// NewDefault creates a new Cli with default configuration.
// It panics if the default configuration fails with an error.
func NewDefault() *Cli {
	c, err := Create()
	if err != nil {
		panic(fmt.Errorf("package cli: Default module had an error: %v", err))
	}
	return c
}
