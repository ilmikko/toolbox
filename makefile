PKGS=$(shell find . -type f -name "*.go" | xargs dirname | sort -u)
GENS=$(shell find . -type f -name "*.gen.*")

build: gen
	go build -o bin/toolbox main/main.go

gen:
	@for gen in $(GENS); do \
		[ -f "$$gen" ] || continue; \
		$$gen; \
	done

test-golden:
	cd golden && ./golden.sh

test-unit:
	go test $(PKGS)

test: gen test-unit build test-golden
