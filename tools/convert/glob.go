// DO NOT TOUCH //
// Generated from ./tools/convert by ./glob.gen.sh //

package convert

import (
	"gitlab.com/ilmikko/toolbox/types/convert/byte"
	"gitlab.com/ilmikko/toolbox/types/convert/percentage"
)

var (
	DefaultConvert = NewDefault()
)

func Bytes2String(n int) string {
	return DefaultConvert.Bytes2String(n)
}

func Bytes(n int, from, to byte.Magnitude) int {
	return DefaultConvert.Bytes(n, from, to)
}

func BytesFloat(f float64, from, to byte.Magnitude) float64 {
	return DefaultConvert.BytesFloat(f, from, to)
}

func BytesString(sfrom, sto string) (int, error) {
	return DefaultConvert.BytesString(sfrom, sto)
}

func BytesStringFloat(sfrom, sto string) (float64, error) {
	return DefaultConvert.BytesStringFloat(sfrom, sto)
}

func Configure(cfg *Config) error {
	return DefaultConvert.Configure(cfg)
}

func Float2Percentage(f float64) percentage.Percentage {
	return DefaultConvert.Float2Percentage(f)
}

func Percentage2Float(p percentage.Percentage) float64 {
	return DefaultConvert.Percentage2Float(p)
}

func Percentage2String(p percentage.Percentage) string {
	return DefaultConvert.Percentage2String(p)
}

func String2Percentage(s string) (percentage.Percentage, error) {
	return DefaultConvert.String2Percentage(s)
}

func Float2String(f float64) string {
	return DefaultConvert.Float2String(f)
}

func Int2String(n int) string {
	return DefaultConvert.Int2String(n)
}

func String2Float(s string) (float64, error) {
	return DefaultConvert.String2Float(s)
}

func String2Int(s string) (int, error) {
	return DefaultConvert.String2Int(s)
}
