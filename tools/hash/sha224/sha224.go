package sha224

type SHA224 struct {
	cmd *Command
}

func (h *SHA224) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *SHA224) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *SHA224 {
	h := &SHA224{}
	h.cmd = NewCommand()
	return h
}
