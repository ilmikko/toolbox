package netinterface

import (
	"fmt"
	"log"
	"net"
	"strings"
)

type State int

func (s State) String() string {
	switch s {
	case STATE_UP:
		return "UP"
	case STATE_DOWN:
		return "DOWN"
	default:
		return "UNKNOWN"
	}
}

const (
	STATE_UNKNOWN State = iota
	STATE_DOWN
	STATE_UP
)

func ParseState(s string) State {
	switch s := strings.ToUpper(s); s {
	case "DOWN":
		return STATE_DOWN
	case "UP":
		return STATE_UP
	case "UNKNOWN":
		return STATE_UNKNOWN
	default:
		log.Printf("Unknown netinterface state: %q", s)
		return STATE_UNKNOWN
	}
}

func ParseAddress(s string) (*net.IP, error) {
	ip, _, err := net.ParseCIDR(s)
	if err != nil {
		return nil, err
	}
	return &ip, nil
}

func ParseAddressSubnet(s string) (*net.IP, *net.IPNet, error) {
	ip, net, err := net.ParseCIDR(s)
	if err != nil {
		return nil, nil, err
	}
	return &ip, net, nil
}

func ParseSubnet(s string) (*net.IPNet, error) {
	_, net, err := net.ParseCIDR(s)
	if err != nil {
		return nil, err
	}
	return net, nil
}

type NetInterface struct {
	Name     string
	State    State
	Address  *net.IP
	Address6 *net.IP
	Subnet   *net.IPNet
	Subnet6  *net.IPNet
}

func (ni *NetInterface) String() string {
	addr := "-"
	addr6 := "-"
	sub := "-"
	sub6 := "-"
	if ni.Address != nil {
		addr = ni.Address.String()
	}
	if ni.Address6 != nil {
		addr6 = ni.Address6.String()
	}
	if ni.Subnet != nil {
		sub = ni.Subnet.String()
	}
	if ni.Subnet6 != nil {
		sub6 = ni.Subnet6.String()
	}
	return fmt.Sprintf("%s %s %s %s %s %s",
		ni.Name,
		ni.State,
		addr,
		addr6,
		sub,
		sub6,
	)
}
