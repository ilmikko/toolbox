package main

import (
	"flag"
	"fmt"
	"log"

	"gitlab.com/ilmikko/toolbox"
	"gitlab.com/ilmikko/toolbox/cli"
)

var (
	flagRoot        = flag.String("root", "", "Use this as the root directory instead of /")
	flagUsage       = flag.Bool("usage", false, "Print out usage information.")
	flagVersion     = flag.Bool("v", false, "")
	flagVersionLong = flag.Bool("version", false, "Print out version information.")
)

func mainErr() error {
	flag.Parse()

	cli, err := cli.Create()
	if err != nil {
		return err
	}

	if *flagVersion || *flagVersionLong {
		fmt.Println(cli.Version())
		return nil
	}

	if *flagUsage {
		fmt.Println(cli.Usage())
		return nil
	}

	cfg := toolbox.NewConfig()
	if f := *flagRoot; f != "" {
		cfg.Root = f
	}

	t := toolbox.New()
	if err := t.Configure(cfg); err != nil {
		return err
	}

	if err := cli.Parse(t, flag.Args()); err != nil {
		return err
	}
	return nil
}

func main() {
	if err := mainErr(); err != nil {
		log.Fatalf("E: %v", err)
	}
}
