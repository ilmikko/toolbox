package cli

import "fmt"

func (c *Cli) Usage() string {
	return fmt.Sprintf("Usage: %s [tool] [args]", c.cfg.Path)
}
