// package toolbox defines the entry point for the Go Toolbox.
package toolbox

import (
	"gitlab.com/ilmikko/toolbox/tools/archive"
	"gitlab.com/ilmikko/toolbox/tools/batteries"
	"gitlab.com/ilmikko/toolbox/tools/convert"
	"gitlab.com/ilmikko/toolbox/tools/disk"
	"gitlab.com/ilmikko/toolbox/tools/file"
	"gitlab.com/ilmikko/toolbox/tools/hash"
	"gitlab.com/ilmikko/toolbox/tools/network"
	"gitlab.com/ilmikko/toolbox/tools/packman"
	"gitlab.com/ilmikko/toolbox/tools/temp"
)

type Config struct {
	Root string
}

type Toolbox struct {
	CFG *Config

	Archive   *archive.Archive
	Batteries *batteries.Batteries
	Convert   *convert.Convert
	Disk      *disk.Disk
	Hash      *hash.Hash
	Network   *network.Network
	Packman   *packman.Packman
	Temp      *temp.Temp
}

func (t *Toolbox) Configure(cfg *Config) error {
	t.CFG = cfg
	{
		bc := batteries.NewConfig()
		// Change battery class root path to align with toolbox root
		bc.Class.Path = file.JoinPathRelative(cfg.Root, bc.Class.Path)
		if err := t.Batteries.Configure(bc); err != nil {
			return err
		}
	}
	{
		nc := network.NewConfig()
		// Change network class resolvconf path to align with toolbox root
		nc.ResolvconfPath = file.JoinPathRelative(cfg.Root, nc.ResolvconfPath)
		if err := t.Network.Configure(nc); err != nil {
			return err
		}
	}
	return nil
}

func Create() (*Toolbox, error) {
	t := New()
	cfg := NewConfig()
	if err := t.Configure(cfg); err != nil {
		return nil, err
	}
	return t, nil
}

func New() *Toolbox {
	t := &Toolbox{}

	t.Archive = archive.New()
	t.Batteries = batteries.New()
	t.Convert = convert.New()
	t.Disk = disk.New()
	t.Hash = hash.New()
	t.Network = network.New()
	t.Packman = packman.New()
	t.Temp = temp.New()

	return t
}
