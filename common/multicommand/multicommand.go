package multicommand

import (
	"bytes"
	"fmt"
	"io"
	"log"

	"gitlab.com/ilmikko/toolbox/common/command"
)

type Command struct {
	Exec      string
	Args      []string
	Translate Translation
}

type Config struct {
	Commands []Command
}

type MultiCommand struct {
	Commands []*command.Command
}

func (c *MultiCommand) RunArgs(args ...string) (string, error) {
	s, err := c.Run(command.Input{
		Args: args,
	})
	if err != nil {
		return "", err
	}
	return s, nil
}

func (c *MultiCommand) RunStdin(r io.Reader) (string, error) {
	s, err := c.Run(command.Input{
		Stdin: r,
	})
	if err != nil {
		return "", err
	}
	return s, nil
}

func (c *MultiCommand) Run(in command.Input) (string, error) {
	if len(c.Commands) == 0 {
		return "", fmt.Errorf("BUG: MultiCmd has no commands to run.")
	}
	for i, cmd := range c.Commands {
		if !command.IsExisting(cmd.Exec) {
			continue
		}
		b := &bytes.Buffer{}
		if in.Stdin != nil {
			in.Stdin = io.TeeReader(in.Stdin, b)
		}
		s, err := cmd.Run(in)
		if err != nil {
			log.Printf("MultiCmd #%d: %v", i, err)
			if in.Stdin != nil {
				in.Stdin = b
			}
			continue
		}
		return s, nil
	}
	return "", fmt.Errorf("MultiCmd ran out of commands due to errors.")
}

func New() *MultiCommand {
	c := &MultiCommand{}
	return c
}
