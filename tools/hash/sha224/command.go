package sha224

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	SHA224RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	sha224sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.sha224sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA224RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.sha224sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA224RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.sha224sum = multicommand.New()
	{
		sha224sum := command.New("sha224sum", "--binary")
		c.sha224sum.Commands = append(c.sha224sum.Commands, sha224sum)
	}
	return c
}
