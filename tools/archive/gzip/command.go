package gzip

import (
	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
)

type Command struct {
	compress *multicommand.MultiCommand
}

func (c *Command) Compress(file string) error {
	if _, err := c.compress.RunArgs(file); err != nil {
		return err
	}
	return nil
}

func NewCommand() *Command {
	c := &Command{}
	c.compress = multicommand.New()
	{
		gzip := command.New("gzip", "--force", "--keep")
		c.compress.Commands = append(c.compress.Commands, gzip)
	}
	return c
}
