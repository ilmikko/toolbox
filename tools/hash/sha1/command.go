package sha1

import (
	"regexp"
	"strings"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
)

var (
	SHA1RX = regexp.MustCompile("[a-f0-9]+")
)

type Command struct {
	sha1sum *multicommand.MultiCommand
}

func (c *Command) Checksum(file string) (string, error) {
	out, err := c.sha1sum.RunArgs(file)
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA1RX)
}

func (c *Command) Hash(s string) (string, error) {
	out, err := c.sha1sum.RunStdin(strings.NewReader(s))
	if err != nil {
		return "", err
	}
	return stdoutparser.MustFindRegex(out, SHA1RX)
}

func NewCommand() *Command {
	c := &Command{}
	c.sha1sum = multicommand.New()
	{
		sha1sum := command.New("sha1sum", "--binary")
		c.sha1sum.Commands = append(c.sha1sum.Commands, sha1sum)
	}
	return c
}
