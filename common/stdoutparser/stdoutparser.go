package stdoutparser

import (
	"fmt"
	"regexp"
	"strings"
)

var (
	TABLE_DELIMITER_RX = regexp.MustCompile(`[\s\t\n]+`)
	EMPTY_LINE_RX      = regexp.MustCompile(`\n\n*`)
	COMMENT_RX         = regexp.MustCompile(`#[^\n]*`)
)

func FindRegex(s string, r *regexp.Regexp) (string, error) {
	return r.FindString(s), nil
}

func MustFindRegex(s string, r *regexp.Regexp) (string, error) {
	find := r.FindString(s)
	if find == "" {
		return "", fmt.Errorf("Could not find regex %q in string %q", r, s)
	}
	return find, nil
}

func NoTrailingNewline(s string) (string, error) {
	return strings.TrimSuffix(s, "\n"), nil
}

func RemoveEmptyLines(s string) (string, error) {
	s = EMPTY_LINE_RX.ReplaceAllString(s, "\n")
	return NoTrailingNewline(s)
}

func RemoveComments(s string) (string, error) {
	s = COMMENT_RX.ReplaceAllString(s, "")
	return RemoveEmptyLines(s)
}

func SplitLines(s string) ([]string, error) {
	return strings.Split(strings.TrimSuffix(s, "\n"), "\n"), nil
}

func SplitLinesTable(s string) ([][]string, error) {
	table := [][]string{}
	lines := strings.Split(strings.TrimSuffix(s, "\n"), "\n")
	for _, line := range lines {
		table = append(table, TABLE_DELIMITER_RX.Split(line, -1))
	}
	return table, nil
}
