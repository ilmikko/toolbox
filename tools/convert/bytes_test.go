package convert_test

import (
	"testing"

	"gitlab.com/ilmikko/toolbox/tools/convert"
)

func TestBytes2String(t *testing.T) {
	testCases := []struct {
		bytes int
		want  string
	}{
		{
			bytes: 1,
			want:  "1 B",
		},
		{
			bytes: 10,
			want:  "10 B",
		},
		{
			bytes: 1000,
			want:  "1 KB",
		},
		{
			bytes: 1024,
			want:  "1.024 KB",
		},
		{
			bytes: 2222,
			want:  "2.222 KB",
		},
		{
			bytes: 10000,
			want:  "10 KB",
		},
		{
			bytes: 1000000,
			want:  "1 MB",
		},
		{
			bytes: 1000001,
			want:  "1.000001 MB",
		},
		{
			bytes: 1000000000,
			want:  "1 GB",
		},
	}

	for _, tc := range testCases {
		got := convert.Bytes2String(tc.bytes)
		want := tc.want
		if got != want {
			t.Errorf("String not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
