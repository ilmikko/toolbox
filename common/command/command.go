package command

import (
	"bytes"
	"fmt"
	"io"
	"os/exec"
	"strings"
)

type Input struct {
	Args  []string
	Stdin io.Reader
}

type Command struct {
	Exec              string
	args              []string
	InputTranslation  func(Input) Input
	OutputTranslation func(string) (string, error)
}

func (c *Command) RunArgs(args ...string) (string, error) {
	s, err := c.Run(Input{
		Args: args,
	})
	if err != nil {
		return "", err
	}
	return s, nil
}

func (c *Command) Run(in Input) (string, error) {
	var e, o bytes.Buffer

	if c.InputTranslation != nil {
		in = c.InputTranslation(in)
	}

	in.Args = append(c.args, in.Args...)
	cmd := exec.Command(c.Exec, in.Args...)
	cmd.Stdin = in.Stdin
	cmd.Stdout = &o
	cmd.Stderr = &e

	if err := cmd.Run(); err != nil {
		errStr := strings.TrimSuffix(e.String(), "\n")
		return "", fmt.Errorf("%s (%v)", errStr, err)
	}

	outStr := o.String()
	if c.OutputTranslation != nil {
		s, err := c.OutputTranslation(outStr)
		if err != nil {
			return "", err
		}
		outStr = s
	}
	return outStr, nil
}

// IsExisting checks whether a given command exists in the PATH variable.
func IsExisting(ex string) bool {
	_, err := exec.LookPath(ex)
	return err == nil
}

func New(ex string, args ...string) *Command {
	c := &Command{}
	c.Exec = ex
	c.args = args
	return c
}
