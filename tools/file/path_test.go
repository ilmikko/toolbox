package file_test

import (
	"testing"

	"gitlab.com/ilmikko/toolbox/tools/file"
)

func TestJoinPath(t *testing.T) {
	testCases := []struct {
		paths []string
		want  string
	}{
		{
			paths: []string{"foo", "bar"},
			want:  "foo/bar",
		},
		{
			paths: []string{"/foo", "bar"},
			want:  "/foo/bar",
		},
		{
			paths: []string{"foo/bar/baz", "baz"},
			want:  "foo/bar/baz/baz",
		},
		{
			paths: []string{"foo/bar/baz", "foo/bar/baz"},
			want:  "foo/bar/baz/foo/bar/baz",
		},
		{
			paths: []string{"foo/", "bar"},
			want:  "foo/bar",
		},
		{
			paths: []string{"foo/", "bar/baz"},
			want:  "foo/bar/baz",
		},
		{
			paths: []string{"////", "", "edge", "/////"},
			want:  "/edge",
		},
		{
			paths: []string{"", ""},
			want:  "",
		},
		{
			paths: []string{"a", "b", "c"},
			want:  "a/b/c",
		},
		{
			paths: []string{"a/b", "b/c"},
			want:  "a/b/b/c",
		},
		{
			paths: []string{"a/b/c", "b/c", "c"},
			want:  "a/b/c/b/c/c",
		},
	}

	for _, tc := range testCases {
		got := file.JoinPath(tc.paths...)
		want := tc.want
		if got != want {
			t.Errorf("Joined path not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}

func TestJoinPathRelative(t *testing.T) {
	testCases := []struct {
		paths []string
		want  string
	}{
		{
			paths: []string{"foo", "bar"},
			want:  "foo/bar",
		},
		{
			paths: []string{"/foo", "bar"},
			want:  "/foo/bar",
		},
		{
			paths: []string{"foo/bar/baz", "baz"},
			want:  "foo/bar/baz",
		},
		{
			paths: []string{"foo/bar/baz", "foo/bar/baz"},
			want:  "foo/bar/baz",
		},
		{
			paths: []string{"foo/", "bar"},
			want:  "foo/bar",
		},
		{
			paths: []string{"foo/", "bar/baz"},
			want:  "foo/bar/baz",
		},
		{
			paths: []string{"////", "", "edge", "/////"},
			want:  "/edge",
		},
		{
			paths: []string{"a", "b", "c"},
			want:  "a/b/c",
		},
		{
			paths: []string{"a/b", "b/c"},
			want:  "a/b/c",
		},
		{
			paths: []string{"a/b/a/b/a/b/a/b", "a/b/c"},
			want:  "a/b/a/b/a/b/a/b/c",
		},
		{
			paths: []string{"a/b/c", "b/c", "c"},
			want:  "a/b/c",
		},
	}

	for _, tc := range testCases {
		got := file.JoinPathRelative(tc.paths...)
		want := tc.want
		if got != want {
			t.Errorf("Joined path not as expected: \n got: %q\nwant: %q", got, want)
		}
	}
}
