package archive

import "gitlab.com/ilmikko/toolbox/types/archive/compression"

const (
	DEFAULT_ARCHIVE     = compression.TAR
	DEFAULT_COMPRESSION = compression.GZIP
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Default.Archive = DEFAULT_ARCHIVE
	cfg.Default.Compression = DEFAULT_COMPRESSION
	return cfg
}
