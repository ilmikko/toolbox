package sha512

type SHA512 struct {
	cmd *Command
}

func (h *SHA512) HashFile(file string) (string, error) {
	sum, err := h.cmd.Checksum(file)
	if err != nil {
		return "", err
	}
	return sum, nil
}

func (h *SHA512) HashString(s string) (string, error) {
	s, err := h.cmd.Hash(s)
	if err != nil {
		return "", err
	}
	return s, nil
}

func New() *SHA512 {
	h := &SHA512{}
	h.cmd = NewCommand()
	return h
}
