package capacity

import (
	"fmt"
	"log"
	"regexp"
	"strconv"

	"gitlab.com/ilmikko/toolbox/common/command"
)

type Capacity struct {
	commands []*command.Command
}

// Bytes returns the total capacity for a disk in bytes.
func (c *Capacity) Bytes(disk string) (int, error) {
	for i, cmd := range c.commands {
		out, err := cmd.RunArgs(disk)
		if err == nil {
			i, err := strconv.Atoi(out)
			if err != nil {
				return -1, err
			}
			return i, nil
		}
		log.Printf("Capacity: Command #%d failed: %v", i, err)
	}
	return -1, fmt.Errorf("No more capacity commands :(")
}

func New() *Capacity {
	c := &Capacity{}
	{
		lsblkCapacity := command.New("lsblk", "--nodeps", "--noheadings", "--bytes", "-o", "NAME,SIZE")
		lsblkCapacity.OutputTranslation = func(out string) (string, error) {
			r := regexp.MustCompile(`\S+\s+(\d+)`)
			m := r.FindStringSubmatch(out)
			if len(m) == 0 {
				return "", fmt.Errorf("No match found for %q", r)
			}
			return m[1], nil
		}
		c.commands = append(c.commands, lsblkCapacity)
	}
	return c
}
