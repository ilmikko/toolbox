// DO NOT TOUCH //
// Generated from ./tools/file by ./glob.gen.sh //

package file

var (
	DefaultFile = NewDefault()
)

func Configure(cfg *Config) error {
	return DefaultFile.Configure(cfg)
}

func List(dir string) ([]string, error) {
	return DefaultFile.List(dir)
}

func JoinPath(paths ...string) string {
	return DefaultFile.JoinPath(paths...)
}

func JoinPathRelative(paths ...string) string {
	return DefaultFile.JoinPathRelative(paths...)
}

func Cat(files ...string) ([]byte, error) {
	return DefaultFile.Cat(files...)
}

func CatString(files ...string) (string, error) {
	return DefaultFile.CatString(files...)
}

func IsDir(path string) bool {
	return DefaultFile.IsDir(path)
}

func MustDir(dir string) error {
	return DefaultFile.MustDir(dir)
}
