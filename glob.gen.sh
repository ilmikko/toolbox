capitalize() {
	awk -vFS="" -vOFS="" '{$1=toupper($1);print $0}';
}

first_letter() {
	awk -vFS="" -vOFS="" '{print $1}';
}

files() {
	for file in ./*; do
		[ -f "$file" ] || continue;
		case "$file" in
			*_test.go)
				;;
			*.go)
				cat "$file";
				;;
		esac
	done
}

go_structify() {
	echo "$1 \*$2";
}

package() {
	awk '/package/ { print $2 }' | uniq -c | sort -r | awk '{ print $2; exit }';
}

imports() {
	awk '{ if (import) if (($1)&&($1!=")")) print $1 } /^\s*$/ { import=0 } /)/ { import=0 } /import/ { import=1; if (($2)&&($2!="(")) print $2 }' | sort | uniq;
}

globgen() {
	DIR="$(dirname "$@")";
	cd "$DIR";
	FILES="$(files)";
	PACKAGE="$(echo "$FILES" | package)";
	IMPORTS="$(echo "$FILES" | imports)";
	GO_STRUCT_ID="$(echo "$PACKAGE" | first_letter)";
	GO_STRUCT_NAME="$(echo "$PACKAGE" | capitalize)";
	GO_STRUCT="$(go_structify "$GO_STRUCT_ID" "$GO_STRUCT_NAME")";
	DEFAULT_ID="Default$GO_STRUCT_NAME";

	cat <<-PACKAGE
	// DO NOT TOUCH //
	// Generated from $DIR by $0 //

	package $PACKAGE;

	PACKAGE
	cat <<-IMPORTS
	import (
		$IMPORTS
	)
	IMPORTS
	cat <<-VARS
	var (
		$DEFAULT_ID=NewDefault()
	)
	VARS
	echo;
	echo "$FILES" |\
		grep "$GO_STRUCT" |\
		sed -e 's/\(func\s*\)(\s*\w*\s*\*\?\w*)\s*/\1/' |\
		perl -slane "$(cat <<YES_ITS_INLINE_PERL
	/func (\w*)\(([^)]*)\)\s*([^{]*){/;
	print "func ",\$1,"(",\$2,") ",\$3," {";
	my @fs = split /,/, \$2;
	foreach my \$arg (@fs) {
		\$arg =~ s/\s*(\S*)\s*(\.\.\.)?.*$/\1\2/;
	}
	print "return ",\$DEFAULT_ID,".",\$1,"(",(join ",", @fs),")";
	print "}\n";
YES_ITS_INLINE_PERL
)" -- "-DEFAULT_ID=$DEFAULT_ID";
}

globgen_all() {
	while [ "$#" -gt 0 ]; do
		globgen "$1" | goimports > "$1";
		shift;
	done
}

PATH="$PATH:$HOME/.go/bin";
GLOBS="$(find . -type f -name "glob.go" | sort -u)";
globgen_all $GLOBS;
