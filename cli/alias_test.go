package cli_test

import (
	"fmt"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/ilmikko/toolbox/cli"
)

func TestAliasPassThrough(t *testing.T) {
	testCases := []struct {
		args    []string
		want    []string
		wantErr error
	}{
		{
			wantErr: fmt.Errorf("No arguments specified."),
		},
		{
			args: []string{"1", "2", "3"},
			want: []string{"1", "2", "3"},
		},
		{
			args: []string{"1"},
			want: []string{"1"},
		},
	}

	c := cli.NewDefault()

	for _, tc := range testCases {
		got, err := c.Alias(tc.args)
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("Error: %v", err)
			}
		}
		want := tc.want
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Args not as expected: \n got: %v\nwant: %v", got, want)
		}
	}
}

func TestAliasRule(t *testing.T) {
	testCases := []struct {
		cli     string
		want    []string
		wantErr error
	}{
		{
			cli:  "batteries",
			want: []string{"batteries", "*", "list"},
		},
		{
			cli:  "batteries %",
			want: []string{"batteries", "*", "percentage"},
		},
		{
			cli:  "batteries stat",
			want: []string{"batteries", "*", "status"},
		},
		{
			cli:  "batteries 1 %",
			want: []string{"batteries", "1", "percentage"},
		},
		{
			cli:  "batteries 0 stat",
			want: []string{"batteries", "0", "status"},
		},
		{
			cli:  "battery",
			want: []string{"batteries", "0", "list"},
		},
		{
			cli:  "battery %",
			want: []string{"batteries", "0", "percentage"},
		},
		{
			cli:  "battery stat",
			want: []string{"batteries", "0", "status"},
		},
		{
			cli:  "battery 0 %",
			want: []string{"batteries", "0", "percentage"},
		},
		{
			cli:  "battery 0 stat",
			want: []string{"batteries", "0", "status"},
		},
		{
			cli:  "convert int",
			want: []string{"convert", "integer"},
		},
		{
			cli:  "convert perc",
			want: []string{"convert", "percentage"},
		},
		{
			cli:  "convert %",
			want: []string{"convert", "percentage"},
		},
		{
			cli:  "convert percentage",
			want: []string{"convert", "percentage"},
		},
		{
			cli:     "disk size",
			wantErr: fmt.Errorf("%s %s", cli.NEEDS_MORE_ARGUMENTS, []string{"disk", "capacity"}),
		},
		{
			cli:  "disk size /dev/sdx",
			want: []string{"disk", "capacity", "/dev/sdx"},
		},
		{
			cli:  "net addr",
			want: []string{"network", "address"},
		},
		{
			cli:  "net dns",
			want: []string{"network", "nameservers"},
		},
		{
			cli:  "net conn foo",
			want: []string{"network", "connection", "foo"},
		},
		{
			cli:  "net conn foo bar",
			want: []string{"network", "connection", "foo", "bar"},
		},
		{
			cli:  "network nameserver",
			want: []string{"network", "nameservers"},
		},
		{
			cli:  "tmp",
			want: []string{"temp"},
		},
	}

	c := cli.NewDefault()

	for _, tc := range testCases {
		got, err := c.Alias(strings.Split(tc.cli, " "))
		if tc.wantErr != nil {
			if err == nil {
				t.Errorf("Expected an error %q but was nil!", tc.wantErr.Error())
			} else {
				got := err.Error()
				want := tc.wantErr.Error()
				if got != want {
					t.Errorf("Error not as expected: \n got: %q\nwant: %q", got, want)
				}
			}
		} else {
			if err != nil {
				t.Errorf("Error: %v", err)
			}
		}
		want := tc.want
		if !reflect.DeepEqual(got, want) {
			t.Errorf("Args not as expected: \n got: %v\nwant: %v", got, want)
		}
	}
}
