package network

import (
	"net"
	"regexp"

	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
	"gitlab.com/ilmikko/toolbox/common/stdoutparser"
	"gitlab.com/ilmikko/toolbox/types/network/nameserver"
	"gitlab.com/ilmikko/toolbox/types/network/netinterface"
)

var (
	HTTP_RESP_RX = regexp.MustCompile(`HTTP/[0-9.]{1,3} ([0-9]{3})`)
)

type Command struct {
	curl       *multicommand.MultiCommand
	netAddr    *multicommand.MultiCommand
	resolvConf *multicommand.MultiCommand
}

func (c *Command) Address() ([]*netinterface.NetInterface, error) {
	out, err := c.netAddr.RunArgs()
	if err != nil {
		return nil, err
	}
	table, err := stdoutparser.SplitLinesTable(out)
	if err != nil {
		return nil, err
	}
	inters := []*netinterface.NetInterface{}
	for _, row := range table {
		inter := &netinterface.NetInterface{
			Name:  row[0],
			State: netinterface.ParseState(row[1]),
		}
		if len(row) > 2 && row[2] != "" {
			inter.Address, inter.Subnet, _ = netinterface.ParseAddressSubnet(row[2])
		}
		if len(row) > 3 && row[3] != "" {
			inter.Address6, inter.Subnet6, _ = netinterface.ParseAddressSubnet(row[3])
		}
		inters = append(inters, inter)
	}
	return inters, nil
}

func (c *Command) Connected(host string) error {
	out, err := c.curl.RunArgs(host)
	if err != nil {
		return err
	}
	if _, err := stdoutparser.MustFindRegex(out, HTTP_RESP_RX); err != nil {
		return err
	}
	return nil
}

func (c *Command) Nameservers() ([]*nameserver.Nameserver, error) {
	var out string
	var err error
	{
		out, err = c.resolvConf.RunArgs()
		if err != nil {
			return nil, err
		}
	}
	{
		out, err = stdoutparser.RemoveComments(out)
		if err != nil {
			return nil, err
		}
	}
	var table [][]string
	{
		table, err = stdoutparser.SplitLinesTable(out)
		if err != nil {
			return nil, err
		}
	}
	nss := []*nameserver.Nameserver{}
	for _, row := range table {
		if len(row) <= 1 {
			continue
		}
		if row[0] != "nameserver" {
			continue
		}
		ip := net.ParseIP(row[1])
		ns := &nameserver.Nameserver{
			Address: &ip,
		}
		nss = append(nss, ns)
	}
	return nss, nil
}

func NewCommand(cfg *Config) *Command {
	c := &Command{}
	c.curl = multicommand.New()
	{
		c.curl.Commands = append(c.curl.Commands,
			command.New("curl", "--head", "--silent", "--show-error"),
		)
	}
	c.netAddr = multicommand.New()
	{
		c.netAddr.Commands = append(c.netAddr.Commands,
			command.New("ip", "-br", "addr"),
		)
	}
	c.resolvConf = multicommand.New()
	{
		// TODO: any way to not use 'cat' and represent this internally as a
		// special command (using file.Cat)?
		c.resolvConf.Commands = append(c.resolvConf.Commands,
			command.New("resolvconf", "-l"),
			command.New("cat", cfg.ResolvconfPath),
		)
	}
	return c
}
