package compression

type Compression int

const (
	// Archiving only
	AR Compression = iota
	CPIO
	TAR
	// Compression only
	BZIP2
	GZIP
	LRZIP
	LZ4
	LZIP
	LZOP
	XZ
	ZSTD
	// Compression & Archiving
	LHASA
	P7ZIP
	RAR
	UNARCHIVER
	ZIP
	ZPAQ
)
