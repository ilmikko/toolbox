package gzip

type Gzip struct {
	cmd *Command
}

// Compress performs a Gzip compression on a file.
func (c *Gzip) Compress(file string) (string, error) {
	if err := c.cmd.Compress(file); err != nil {
		return "", err
	}
	return file + ".gz", nil
}

func (c *Gzip) CompressAll(files ...string) ([]string, error) {
	fs := []string{}
	for _, file := range files {
		f, err := c.Compress(file)
		if err != nil {
			return fs, err
		}
		fs = append(fs, f)
	}
	return fs, nil
}

func New() *Gzip {
	c := &Gzip{}
	c.cmd = NewCommand()
	return c
}
