package file

import (
	"path/filepath"
	"strings"
)

// JoinPath joins two or more paths.
// JoinPath("a/b", "b/c") -> "a/b/b/c"
// JoinPath("a/b/c", "b/c", "c") -> "a/b/c/b/c/c"
// JoinPath("c", "a/b/c") -> "c/a/b/c"
func (f *File) JoinPath(paths ...string) string {
	return filepath.Join(paths...)
}

// JoinPath joins two or more paths if they are not relative to each other.
// JoinPath("a/b", "b/c") -> "a/b/c"
// JoinPath("a/b/c", "b/c", "c") -> "a/b/c"
// JoinPath("c", "a/b/c") -> "c/a/b/c"
func (f *File) JoinPathRelative(paths ...string) string {
	dirs := []string{}
	for _, path := range paths {
		path = filepath.Clean(path)
		if strings.HasPrefix(path, "/") {
			dirs = append(dirs, "/")
		}
		pathdirs := strings.Split(path, "/")
		for _, dir := range dirs {
			if len(pathdirs) == 0 {
				break
			}
			if pathdirs[0] == "" || pathdirs[0] == dir {
				pathdirs = pathdirs[1:]
			}
		}
		dirs = append(dirs, pathdirs...)
	}
	return f.JoinPath(dirs...)
}
