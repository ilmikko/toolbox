package file

import (
	"os"
)

func (f *File) List(dir string) ([]string, error) {
	d, err := os.Open(dir)
	if err != nil {
		return nil, err
	}

	fs, err := d.Readdirnames(0)
	if err != nil {
		return nil, err
	}

	return fs, nil
}
