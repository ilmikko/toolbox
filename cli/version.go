// DO NOT TOUCH //
// Generated from git repo by ./cli/version.go.gen.sh //
package cli

import "fmt"

const (
	VERSION = "v1.0.31 (4dd8bd8a)"
)

func (c *Cli) Version() string {
	return fmt.Sprintf("Version %s", VERSION)
}
