package convert

import "fmt"

// Config holds the configuration for a Convert.
type Config struct{}

// Configure configures a Convert with a Config.
func (c *Convert) Configure(cfg *Config) error {
	return nil
}

// NewConfig creates a new default configuration.
func NewConfig() *Config {
	return &Config{}
}

// NewDefault creates a new Convert with default configuration.
// It panics if the default configuration fails with an error.
func NewDefault() *Convert {
	c, err := Create()
	if err != nil {
		panic(fmt.Errorf("package convert: Default module had an error: %v", err))
	}
	return c
}
