package packman

import (
	"gitlab.com/ilmikko/toolbox/common/command"
	"gitlab.com/ilmikko/toolbox/common/multicommand"
)

type Command struct {
	install *multicommand.MultiCommand
}

func (c *Command) Install(pkg string) error {
	if _, err := c.install.RunArgs(pkg); err != nil {
		return err
	}
	return nil
}

func NewCommand() *Command {
	c := &Command{}
	c.install = multicommand.New()
	{
		apt := command.New("apt", "install")
		c.install.Commands = append(c.install.Commands, apt)
	}
	{
		aptGet := command.New("apt-get", "install")
		c.install.Commands = append(c.install.Commands, aptGet)
	}
	{
		pacman := command.New("pacman", "--sync", "--refresh", "--noconfirm")
		c.install.Commands = append(c.install.Commands, pacman)
	}
	{
		rpm := command.New("rpm", "--install")
		c.install.Commands = append(c.install.Commands, rpm)
	}
	{
		yum := command.New("yum", "install")
		c.install.Commands = append(c.install.Commands, yum)
	}
	return c
}
