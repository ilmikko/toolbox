package algorithm

type Algorithm int

const (
	ADLER32 Algorithm = iota
	CRC32
	CRC64
	FNV
	MD5
	SHA1
	SHA224
	SHA256
	SHA384
	SHA512
)
