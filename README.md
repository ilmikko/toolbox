# Toolbox

A tool box to unify common Unix tasks.

Currently supported features include:

* Archival
	* Traditional archival as TAR
	* Compression using several algorithms

* Hashing
	* Binary data or strings
	* Checksums for files

A better description will follow, but for now check tests in ./golden for usage examples.
