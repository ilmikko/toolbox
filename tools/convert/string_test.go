package convert

import (
	"testing"

	"gitlab.com/ilmikko/toolbox/tools/convert"
)

func TestString2Int(t *testing.T) {
	testCases := []struct {
		str  string
		want int
	}{
		{
			str:  "1",
			want: 1,
		},
		{
			str:  "100",
			want: 100,
		},
		{
			str:  "-100",
			want: -100,
		},
		{
			str:  "-10.0",
			want: -10,
		},
		{
			str:  "a1234",
			want: 1234,
		},
		{
			str:  "q 10 00",
			want: 10,
		},
	}

	for _, tc := range testCases {
		got, _ := convert.String2Int(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("Int not as expected: \n got: %d\nwant: %d", got, want)
		}
	}
}

func TestString2Float(t *testing.T) {
	testCases := []struct {
		str  string
		want float64
	}{
		{
			str:  "1",
			want: 1,
		},
		{
			str:  "100",
			want: 100.0,
		},
		{
			str:  "-100.0",
			want: -100.0,
		},
		{
			str:  "-10.01",
			want: -10.01,
		},
		{
			str:  "a12.34",
			want: 12.34,
		},
		{
			str:  "q-1000.421",
			want: -1000.421,
		},
	}

	for _, tc := range testCases {
		got, _ := convert.String2Float(tc.str)
		want := tc.want
		if got != want {
			t.Errorf("Float not as expected: \n got: %f\nwant: %f", got, want)
		}
	}
}
