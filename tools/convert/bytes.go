package convert

import "gitlab.com/ilmikko/toolbox/types/convert/byte"

// Bytes2String converts a number of bytes to a human readable string.
func (c *Convert) Bytes2String(n int) string {
	return byte.ToStringBytes(n)
}

// TODO: Clarify the API below.

// Bytes converts an integer of one magnitude to another.
func (c *Convert) Bytes(n int, from, to byte.Magnitude) int {
	b := byte.ToBytes(n, from)
	return byte.ToMagnitude(b, to)
}

// BytesFloat converts a float of one magnitude to another.
func (c *Convert) BytesFloat(f float64, from, to byte.Magnitude) float64 {
	b := byte.ToBytesFloat(f, from)
	return byte.ToMagnitudeFloat(b, to)
}

// BytesString converts a string such as 1MiB to an integer of another magnitude.
func (c *Convert) BytesString(sfrom, sto string) (int, error) {
	b, from, err := byte.Parse(sfrom)
	if err != nil {
		return 0, err
	}
	_, to, err := byte.Parse(sto)
	if err != nil {
		return 0, err
	}

	n := c.Bytes(b, from, to)
	return n, nil
}

// BytesStringFloat converts a string such as 1MiB to a float of another magnitude.
func (c *Convert) BytesStringFloat(sfrom, sto string) (float64, error) {
	b, from, err := byte.ParseFloat(sfrom)
	if err != nil {
		return 0, err
	}
	_, to, err := byte.ParseFloat(sto)
	if err != nil {
		return 0, err
	}

	n := c.BytesFloat(b, from, to)
	return n, nil
}
