package percentage

import (
	"math"

	"gitlab.com/ilmikko/toolbox/types/convert/number"
)

type Percentage float64

// Float converts a Percentage to a float between 0..1.
func (p Percentage) Float() float64 {
	return float64(p)
}

// String converts a Percentage to a string, such as 99.4%.
func (p Percentage) String() string {
	return number.Float2String(float64(p*100.0)) + "%"
}

// ApplyInt applies a Percentage to an integer.
func (p Percentage) ApplyInt(i int) int {
	return int(math.Round(p.Float() * float64(i)))
}

// ApplyString applies a Percentage to a string.
// ApplyString(50%, "100 apples") -> "50 apples"
// ApplyString(50%, "120 euros") -> "60 euros"
// ApplyString(10%, "1000KB") -> "100KB"
func (p Percentage) ApplyString(s string) (string, error) {
	prefix := ""
	suffix := ""
	// Retain the prefix and suffix
	ms := number.FloatRX.Split(s, 2)
	switch {
	case len(ms) <= 0:
	case len(ms) <= 1:
		prefix = ms[0]
	case len(ms) <= 2:
		prefix = ms[0]
		suffix = ms[1]
	}

	pf, err := number.String2Float(s)
	if err != nil {
		return "", err
	}
	return prefix + number.Float2String(p.Float()*pf) + suffix, nil
}

// Float2Percentage converts a float to a percentage.
func Float2Percentage(f float64) Percentage {
	return Percentage(f)
}

// Percentage2Float converts a percentage to a float.
// You can also simply call p.Float() to get the value.
func Percentage2Float(p Percentage) float64 {
	return p.Float()
}

// Percentage2String converts a percentage to a string.
// You can also simply call p.String() to get the value.
func Percentage2String(p Percentage) string {
	return p.String()
}

// String2Percentage converts a string into a Percentage.
// A % sign is optional.
// Any representation other than numbers will be lost.
func String2Percentage(s string) (Percentage, error) {
	pf, err := number.String2Float(s)
	if err != nil {
		return 0, err
	}
	return Percentage(pf / 100.0), nil
}
