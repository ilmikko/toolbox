package hash

import (
	"fmt"

	"gitlab.com/ilmikko/toolbox/types/hash/algorithm"
	"gitlab.com/ilmikko/toolbox/types/hash/hasher"
)

type Config struct {
	Default struct {
		Algorithm algorithm.Algorithm
		Checksum  algorithm.Algorithm
	}
}

type Hash struct {
	cfg     *Config
	hashers map[algorithm.Algorithm]hasher.Hasher
}

func (c *Hash) Checksum(files ...string) ([]string, error) {
	fs, err := c.ChecksumWith(c.cfg.Default.Checksum, files...)
	if err != nil {
		return fs, err
	}
	return fs, nil
}

func (c *Hash) ChecksumWith(a algorithm.Algorithm, files ...string) ([]string, error) {
	hr, ok := c.hashers[a]
	if !ok {
		return nil, fmt.Errorf("No hasher found for algorithm %q", a)
	}
	hs := []string{}
	for _, file := range files {
		h, err := hr.HashFile(file)
		if err != nil {
			return hs, err
		}
		hs = append(hs, h)
	}
	return hs, nil
}

func (c *Hash) String(s string) (string, error) {
	hs, err := c.StringWith(c.cfg.Default.Algorithm, s)
	if err != nil {
		return "", err
	}
	return hs, nil
}

func (c *Hash) StringWith(a algorithm.Algorithm, s string) (string, error) {
	hr, ok := c.hashers[a]
	if !ok {
		return "", fmt.Errorf("No hasher found for algorithm %q", a)
	}
	hs, err := hr.HashString(s)
	if err != nil {
		return "", err
	}
	return hs, nil
}

func Create(cfg *Config) (*Hash, error) {
	c := New()
	c.cfg = cfg
	return c, nil
}

func New() *Hash {
	c := &Hash{}
	c.cfg = NewConfig()
	c.hashers = hasher.Defaults
	return c
}
