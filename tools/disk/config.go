package disk

import "gitlab.com/ilmikko/toolbox/types/convert/byte"

const (
	DEFAULT_MAGNITUDE = byte.BYTE
)

func NewConfig() *Config {
	cfg := &Config{}
	cfg.Default.Magnitude = DEFAULT_MAGNITUDE
	return cfg
}
